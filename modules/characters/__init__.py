from discord.ext import commands
from discord.ext.commands.cog import Cog
from discord import utils
from discord import File
import asyncio
from tempfile import mkstemp
from os import fdopen


class Characters(Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.group()
    async def c(self, ctx):
        """Character management commands"""
        pass

    @c.command()
    async def create(self, ctx):
        """
        Begin approval process for a character.
        Usage:
        rp!c create
        [charsheet]
        """
        channel = utils.find(lambda c: c.name == "oc-submission", ctx.guild.channels).mention
        if ctx.message.channel.name != "oc-submission":
            sent_msg = await ctx.send(f"{ctx.author.mention}, please, post the character sheet in"
                                      f" {channel}.\n"
                                      f"Your original message will be deleted in 10 seconds.")
            await asyncio.sleep(10)
            await ctx.message.delete()
            await sent_msg.delete()
            return
        await ctx.message.add_reaction("\N{White Heavy Check Mark}")
        await ctx.message.add_reaction("\N{Negative Squared Cross Mark}")
        #approver_role = utils.find(lambda r: r.name == "Character Approver", ctx.guild.roles)
        #mention = await ctx.send(approver_role.mention)
        await asyncio.sleep(1)
        #await mention.delete()

    @c.command()
    @commands.has_any_role("Co-Owner")
    async def force_approve(self, ctx, msg_id):
        message = utils.find(lambda c: c.name == "oc-submission", ctx.guild.channels)
        message = await message.get_message(msg_id)
        channel = utils.find(lambda c: c.name == "oc-approved", ctx.guild.channels)
        attachments = []
        for atmnt in message.attachments:
            f, name = mkstemp(suffix=atmnt.filename)
            fs = fdopen(f, "wb")
            await atmnt.save(fs)
            fs = File(name)
            attachments.append(fs)
        await channel.send(message.content.replace("rp!c create", "")+f"\n\nCharacter by: {message.author.mention}", files=attachments)
        await message.delete()
        await ctx.message.delete()
        player_role = utils.find(lambda r: r.name == "Roleplayer", message.guild.roles)
        if player_role not in message.author.roles:
            await message.author.add_roles(player_role, "Has an approved character.")


def setup(bot):
    bot.add_cog(Characters(bot))

    async def handle_votes(reaction, user):
        print("on_reaction_add")
        if reaction.message.channel.name != "oc-submission":
            return
        print("New reaction!")
        confirmed = utils.find(lambda r: r.name == "Character Approver", user.roles)
        print(confirmed)
        if confirmed is not None:
            try:
                count_yea, count_nay = utils.find(lambda r: r.emoji == "\N{White Heavy Check Mark}",
                                                  reaction.message.reactions).count,\
                                       utils.find(lambda r: r.emoji == "\N{Negative Squared Cross Mark}",
                                                  reaction.message.reactions).count
            except AttributeError:
                print("Can't process reactions, skipping.")
                return
            
            if count_nay*2 < count_yea:
                player_role = utils.find(lambda r: r.name == "Roleplayer", reaction.message.guild.roles)
                channel = utils.find(lambda c: c.name == "oc-approved", reaction.message.guild.channels)
                attachments = []
                for atmnt in reaction.message.attachments:
                    f, name = mkstemp(suffix=atmnt.filename)
                    fs = fdopen(f, "wb")
                    await atmnt.save(fs)
                    fs = File(name)
                    attachments.append(fs)
                await channel.send(reaction.message.content.replace("rp!c create", "")+f"\n\nCharacter by: {reaction.message.author.mention}", files=attachments)
                await reaction.message.delete()
                if player_role not in reaction.message.author.roles:
                    await reaction.message.author.add_roles(player_role, "Has an approved character.")
        else:
            await asyncio.sleep(0.2)
            await reaction.message.remove_reaction(reaction.emoji, user)
            message = await reaction.message.channel.send(f"{user.mention}, you don't have the required role to vote on"
                                                          f" characters.")
            await asyncio.sleep(10)
            await message.delete()

    @bot.event
    async def on_raw_reaction_add(payload):
        print("on_raw_reaction_add")
        message = await bot.get_channel(payload.channel_id).get_message(payload.message_id)
        reaction = utils.find(lambda r: r.emoji == payload.emoji.name, message.reactions)
        user = bot.get_guild(payload.guild_id).get_member(payload.user_id)
        await handle_votes(reaction, user)
