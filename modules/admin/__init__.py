from discord.ext import commands
from discord.ext.commands.cog import Cog
import asyncio


class Owners(Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.group()
    @commands.has_any_role("Co-Owner")
    async def o(self, ctx):
        """Owner only commands"""
        pass

    @o.error
    async def a_error(self, ctx, *args):
        await ctx.send("You don't have the required role for this server.")

    @o.command()
    async def announce(self, ctx, *msg):
        """
        Post an announcement, pinging all members of the server that posted a message in the past month.
        """
        await ctx.send("Not implemented.")
        await ctx.send("But hey. I'll just echo the message in the meantime:")
        await ctx.send(" ".join(map(str, msg)))

    @o.command()
    async def purge(self, ctx, count=100):
        deleted = await ctx.channel.purge(limit=count)
        message = await ctx.send(f"Deleted {len(deleted)} messages.")
        await asyncio.sleep(3)
        await message.delete()


def setup(bot):
    bot.add_cog(Owners(bot))
