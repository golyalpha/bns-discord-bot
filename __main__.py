#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import discord
from discord.ext import commands
from discord.ext.commands.errors import NoEntryPointError
from discord import utils
from os import walk, getenv
from os.path import join as pjoin
from uuid import uuid4
#try:
#    import sentry_sdk
#    sentry_sdk.init("https://39ee46de16ed4616a0f62575091875ca@sentry.io/1389501")
#except ImportError:
print("Sentry reporting not available.")


try:
    sid = getenv("SHARD_ID")
except KeyError:
    sid = str(uuid4())

__doc__ = f"""
Fairmarble Bot
---------------------------------------------------------
This instance of Fairmarble Bot is running on shard ID {sid}
---------------------------------------------------------
"""

bot = commands.Bot(command_prefix="rp!", description=__doc__, activity=discord.Game("rp!help"))


@bot.event
async def on_ready():
    print("Ready!")


print()
print("Begin module loader")
print()
for root, dirs, files in walk("modules"):
    del files
    for module in dirs:
        path = pjoin(root, module).replace("/", ".").replace("\\",".")
        print(f"Loading {path}")
        try:
            bot.load_extension(path)
            print("Loaded.")
        except ModuleNotFoundError:
            print("Module not found.")
            print("Ignoring.")
        except NoEntryPointError:
            print("Malformed module.")
            print("Ignoring.")
        except discord.errors.ClientException as e:
            print("Malformed module, %s." % e)
            print("Ignoring.")
print()
print("End module loader")
print()
print(discord.__version__)
bot.run(getenv("DISCORD_BOT_API"))
